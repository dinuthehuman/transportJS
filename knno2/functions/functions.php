<?php

//aktuelle URL lesen
function currURL() {
    $pageURL = 'http';
    if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
	$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    } else {
	$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}

//URL von GET-Parametern befreien
function noparamURL($url) {
    $url = strtok($url, '?');
    return $url;
}

//GET-Parameter ändern/hinzufügen, falls noch nicht vorhanden
function changeParam() {
    $arguments = func_get_args();
    $newParams = $_GET;
    for($n = 1; $n < count($arguments); $n+=2) {
	$newParams[$arguments[$n]] = $arguments[$n+1];
    }
    return noparamURL($arguments[0]).'?'.http_build_query($newParams);
}

//GET-Parameter löschen
function unsetParam() {
    $arguments = func_get_args();
    $newParams = $_GET;
    for($n = 1; $n < count($arguments); $n+=1) {
	unset($newParams[$arguments[$n]]);
    }
    return noparamURL($arguments[0]).'?'.http_build_query($newParams);
}


function movenumbers ($ALLpersID,$ALLordern,$direct,$nowID) {
    $nowkey=array_search($nowID,$ALLpersID);
    $nowordern=$ALLordern[$nowkey];
    $nextkey=$nowkey+$direct;
    if(array_key_exists($nextkey,$ALLpersID)) {
	$nextID=$ALLpersID[$nextkey];
	$nextordern=$ALLordern[$nextkey];
    }
    else {
	$nextID=$ALLpersID[$nowkey];
	$nextordern=$ALLordern[$nowkey];
    }
    $output[]=$nowID;
    $output[]=$nextordern;
    $output[]=$nextID;
    $output[]=$nowordern;
    return $output;
}

function sECONDS_TO_DHMS($seconds) {
    
    if(false) {return 'neg.';}
    else {
    $days = floor($seconds/86400);
    $hrs = floor($seconds/3600);
    $mins = intval(($seconds / 60) % 60); 
    $sec = intval($seconds % 60);

    if($days>0) {
	//echo $days;exit;
	$hrs = str_pad($hrs,2,'0',STR_PAD_LEFT);
	$hours=$hrs-($days*24);
	$return_days = $days." Days ";
	$hrs = str_pad($hours,2,'0',STR_PAD_LEFT);
    }
    
    else {
	$return_days="";
	$hrs = str_pad($hrs,2,'0',STR_PAD_LEFT);
    }

    $mins = str_pad($mins,2,'0',STR_PAD_LEFT);
    $sec = str_pad($sec,2,'0',STR_PAD_LEFT);

    return $return_days.$hrs.":".$mins.":".$sec;
    }
}

?>