<?php

class transportQuery {
    
    /*Query-Variabeln*/
    private $start = "";
    private $dest = "";
    private $limit = 3;
    private $via = "";
    private $transportations = array(); //mögliche Werte: ice_tgv_rj, ec_ic, ir, re_d, ship, s_sn_r, bus, cableway, arz_ext, tramway_underground
    
    /*Query-Operatoren*/
    private $queryURL = "";
    private $queryFeedback = "";
    private $decodedFeedback = array();
    
    /*Layout Vars*/
    private $spanPrefix = '';
    private $title = '';
    
    /*Javascript-Speicher*/
    public $JSfunctions = '';
    
    public function setRoute($s,$d) {
	$this->start = $s;
	$this->dest = $d;
    }
    
    public function setLimit($l) {
	$this->limit = $l;
    }
    
    public function setVia($t) {
	$this->via = $t;
    }
    
    public function addTransportation($m){
	$this->transportations[] = $m;
    }
    
    public function setPrefix($a) {
	$this->spanPrefix = $a;
    }
    
    public function setTitle($t) {
	$this->title = $t;
    }
    
    private function constructURL() {
	$this->queryURL = "http://transport.opendata.ch/v1/connections";
	$this->queryURL .= "?from=";
	$this->queryURL .= $this->start;
	$this->queryURL .= "&to=";
	$this->queryURL .= $this->dest;
	if(!empty($this->via)) {
	    $this->queryURL .= "&via=";
	    $this->queryURL .= $this->via;
	}
	if(sizeof($this->transportations) > 0) {
	    foreach($this->transportations as $value) {
		$this->queryURL .= "&transportations[]=";
		$this->queryURL .= $value;
	    }
	}
	$this->queryURL .= "&limit=";
	$this->queryURL .= $this->limit+1;
	
    }
    
    public function execQuery() {
	$this->constructURL();
	$this->queryFeedback = $this->curl_download($this->queryURL);
	$this->decodedFeedback = json_decode($this->queryFeedback, true);
    }
    
    public function printOutput() {
	if($this->decodedFeedback['connections'][0]['from']['departureTimestamp']-time() > 0) $add = 0;
	else $add = 1;
	
	echo "<fieldset><legend>";
	echo $this->title;
	echo "</legend>";
	
	$JSfunctions = "";
	
	for($n=0; $n < $this->limit; $n++) {
	    
	    $this->JSfunctions .= "setInterval(function() {countDown(".$this->decodedFeedback['connections'][$n+$add]['from']['departureTimestamp'].",'".$this->spanPrefix.$n."');},1000);";
	    ?>
	    
	    <div class='countDownWrapper'>
		<span id="<?php echo $this->spanPrefix.$n; ?>"></span>&nbsp;|
		<?php
		echo date("G:i",$this->decodedFeedback['connections'][$n+$add]['from']['departureTimestamp']);
		echo " -> ";
		echo date("G:i",$this->decodedFeedback['connections'][$n+$add]['to']['arrivalTimestamp']);
		?>
	    </div>
	    <?php
	}
	?>
	
	</fieldset>
	<?php
    }
    /*
    public function testOut() {
	echo "<pre>";
	//var_dump($this->decodedFeedback);
	//var_dump(array_keys($this->decodedFeedback['connections'][0]['to']));
	var_dump($this->decodedFeedback['connections'][0]['to']['arrivalTimestamp']);
	echo "</pre>";
	//echo $this->decodedFeedback['connections'][0]['departureTimestamp'];
	//var_dump(array_keys($this->decodedFeedback['connections'][0]['from']));
    }
    */
    
    /*Query-Download Programm*/
    private function curl_download($URL) {
	
	$con = curl_init();
	curl_setopt($con, CURLOPT_URL, $URL);
	curl_setopt($con, CURLOPT_RETURNTRANSFER, true);
	$output = curl_exec($con);
	curl_close($con);
	return $output;
	
    }
    
    
}
