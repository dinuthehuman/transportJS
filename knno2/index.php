<!DOCTYPE html>
<html>

<head>
    <!--META-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="refresh" content="120">
    <title>K-NNO</title>
    
    <!--CSS-->
    <link href='https://fonts.googleapis.com/css?family=Oxygen:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Anonymous+Pro:400,700' rel='stylesheet' type='text/css'>
    <link href="style.css" rel="stylesheet" type="text/css" />
    
    <!--JS-->
    <script type="text/javascript" src="functions/JSfunctions.js"></script>
</head>

<body id="wrapper" onload="startClock();">
    <div class="center" id="clock"></div>
    <div id="dateWrapper"><span id="day"></span><span id="date"></span></div>
    <iframe src="transportModule.php" style="width: 600px; height: 260px; border: none;"></iframe>
    <iframe src="weatherModule.php" style="height: 430px; width: 226px; overflow: hidden;"></iframe>
</body>

</html>