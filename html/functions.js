$(function updateCountdowns(){
    $("fieldset").each(function(index){
        
        var countdownIDs = [];
        $(this).children(".countDownWrapper").children(".countdown").each(function(){
           countdownIDs.push($(this).attr('id'));
        });
        
        var url = "http://transport.opendata.ch/v1/connections?limit=4&from=" + ($(this).children("code.from").html());
        url += "&to=" + ($(this).children("code.to").html());
        
        var url_via = $(this).children("code.via").html();
        if(!isEmpty(url_via)) url += "&via=" + url_via;
        
        var url_trans = $(this).children("code.transportations").html();
        if(!isEmpty(url_trans)) url += "&transportations[]=" + url_trans;
        console.log(Date.now());
        $.ajax({
            url: url,
            success: function(result) {
                var nAdd = 0;
                if(result["connections"][0]["from"]["departureTimestamp"] <= Math.floor(Date.now() / 1000)) {nAdd=1};
                console.log(nAdd);
                for(var n = 0; n < 3; n++) {
                    initializeClock(countdownIDs[n], result["connections"][n+nAdd]["from"]["departureTimestamp"])
                    if(result["connections"][n+nAdd]["from"]["departureTimestamp"] <= Math.floor(Date.now() / 1000)) {
                        updateCountdowns();
                        break;
                    }
                }
                //var resultObj = result["connections"][0]["from"]["departureTimestamp"];
                //console.log(resultObj);
            }
        });
        console.log(url);
    });
});


function isEmpty(value){
  return (value == null || value.length === 0);
}

function initializeClock(id, endtime) {
    var clock = document.getElementById(id);
    var hoursSpan = clock.querySelector('.hours');
    var minutesSpan = clock.querySelector('.minutes');
    var secondsSpan = clock.querySelector('.seconds');
    
    function updateClock() {
        var t = getTimeRemaining(endtime);
        
        hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
        minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
        secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);
        
        if (t.total <= 0) {
            clearInterval(timeinterval);
        }
    }

    updateClock();
    var timeinterval = setInterval(updateClock, 1000);
}

function getTimeRemaining(endtime){
    var t = endtime -  Math.floor(Date.now() / 1000);
    var seconds = Math.floor(t % 60);
    var minutes = Math.floor((t/60) % 60);
    var hours = Math.floor((t/(60*60)) % 24 );
    return {
        'total': t,
        'hours': hours,
        'minutes': minutes,
        'seconds': seconds
    };
}