//////////////////////////
// UHR-FUNKTIONEN
//////////////////////////

function startClock() {
    var today = new Date();
    document.getElementById('clock').innerHTML = today.getHours() + ':' + zeroPad(today.getMinutes());
    document.getElementById('day').innerHTML = weekdays(today.getDay()) + '/';
    document.getElementById('date').innerHTML = zeroPad(today.getDate()) + '/' + zeroPad(today.getMonth()+1) + '';
    var t = setTimeout(startClock, 500);
}

function zeroPad(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}

function weekdays(n) {
    switch (n) {
        case 0: return 'Mo'; break;
        case 1: return 'Tu'; break;
        case 2: return 'We'; break;
        case 3: return 'Th'; break;
        case 4: return 'Fr'; break;
        case 5: return 'Sa'; break;
        case 6: return 'Su'; break;
        
    }
}

//////////////////////////
// COUNTDOWN-FUNKTIONEN
//////////////////////////

function countDown(timestamp, tagID) {
    if (!Date.now) {
        Date.now = function() { return new Date().getTime(); }
    }  
    currTimestamp = Math.floor(Date.now() / 1000);
    toGo = timestamp - currTimestamp;
    if (toGo <= 0) {
        location.reload(true);
    }
    else {
        document.getElementById(tagID).innerHTML = convertSeconds(toGo);
    }
}

function convertSeconds(secs) {
    var hours = Math.floor(secs/3600);
    var minutes = Math.floor((secs % 3600)/60);
    var seconds = ((secs % 3600) % 60);
    return hours + ':' + zeroPad(minutes) + ':' + zeroPad(seconds);
}